package com.finn.man.financeMan.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * V.B. on
 * 28-Feb-18.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Category")
@Table(name = "categories")
public class Category extends BaseEntity {

    @Column(name = "description", unique = true)
    private String description;
}
