package com.finn.man.financeMan.model;

/**
 * V.B. on
 * 06-Feb-18.
 */
public enum OperationType {
    INCOME, OUTCOME
}
