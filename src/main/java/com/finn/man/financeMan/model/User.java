package com.finn.man.financeMan.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.finn.man.financeMan.to.CustomDateSerializer;
import com.finn.man.financeMan.to.JsonBalanceInUserSerializer;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * V.B. on
 * 23-Feb-18.
 */
@JsonIgnoreProperties(value = {"new", "password", "hibernateLazyInitializer", "handler"})
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "User")
@Table(name = "users")
public class User extends BaseEntity {

    @Column(name = "name")
    private String name;
    @Column(name = "age")
    private int age;

    @Column(name = "email", nullable = false, unique = true)
    @Email
    @NotBlank
    @Size(max = 100)
    private String email;

    @Column(name = "password", nullable = false)
    @NotBlank
    @Size(min = 5, max = 64)
    private String password;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "owner")
    @JsonSerialize(using = JsonBalanceInUserSerializer.class)
    private Balance balance;

    @Column(name = "enabled", nullable = false, columnDefinition = "bool default true")
    @NotNull
    private boolean enabled = true;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_date", nullable = false, updatable = false)
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date registrationDate;
}
