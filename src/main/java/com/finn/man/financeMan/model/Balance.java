package com.finn.man.financeMan.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * V.B. on
 * 03-Mar-18.
 */
@EqualsAndHashCode(callSuper = false)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Balance")
@Table(name = "balances")
public class Balance extends BaseEntity {

    @JsonIgnore
    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "user_id")
    private User owner;

    @Column(name = "amount")
    private BigDecimal amount;

    @Override
    public String toString() {
        return "Balance{" +
                "owner=" + owner.getName() +
                ", amount=" + amount +
                '}';
    }
}
