package com.finn.man.financeMan.model;

import lombok.Data;

import javax.persistence.*;

/**
 * V.B. on
 * 03-Mar-18.
 */
@Data
@MappedSuperclass
public class BaseEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public boolean isNew() {
        return this.id == null;
    }
}
