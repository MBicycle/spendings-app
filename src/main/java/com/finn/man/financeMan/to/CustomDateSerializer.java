package com.finn.man.financeMan.to;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.util.Date;

/**
 * V.B. on
 * 08-Feb-18.
 */
@JsonComponent
public class CustomDateSerializer extends JsonSerializer<Date> {

//    private SimpleDateFormat formatter =
//            new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");


    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeNumber(date.getTime());
    }
}
