package com.finn.man.financeMan.to.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * V.B. on
 * 13-Mar-18.
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CreateUserInputResource extends AuthenticateUserInputResource {
    @NotNull
    @Size(min = 3, max = 50)
    private String name;
    @NotNull
    @Min(8)
    @Max(300)
    private int age;
    @Email
    @NotNull
    private String email;
    @NotNull
    @Size(min = 5, max = 64)
    private String password;
}
