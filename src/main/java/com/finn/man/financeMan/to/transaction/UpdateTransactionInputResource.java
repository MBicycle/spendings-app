package com.finn.man.financeMan.to.transaction;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * V.B. on
 * 29-Mar-18.
 */
@Data
public class UpdateTransactionInputResource {

    @NotNull
    private Long id;

    @NotNull
    @Size(min = 4, max = 7)
    private String operationType;

    @Size(max = 255)
    private String category;

    @NotNull
    private BigDecimal amount;

    @NotNull
    @Size(max = 500)
    private String description;
}
