package com.finn.man.financeMan.to;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.finn.man.financeMan.model.Balance;

import java.io.IOException;

/**
 * V.B. on
 * 13-Mar-18.
 */
public class JsonBalanceInUserSerializer extends StdSerializer<Balance> {

    public JsonBalanceInUserSerializer() {
        this(null);
    }

    private JsonBalanceInUserSerializer(Class<Balance> t) {
        super(t);
    }

    @Override
    public void serialize(
            Balance value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {

        jgen.writeStartObject();
        jgen.writeNumberField("id", value.getId());
        jgen.writeEndObject();
    }
}

