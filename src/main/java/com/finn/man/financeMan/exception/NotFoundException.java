package com.finn.man.financeMan.exception;

/**
 * V.B. on
 * 09-Mar-18.
 */
public class NotFoundException extends ExpectedException {
    public NotFoundException(String description) {
        super(description);
    }
}
