package com.finn.man.financeMan.exception;

//@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class TransactionNotFoundException extends RuntimeException {


    public TransactionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionNotFoundException() {
        super("Transaction not found");
    }

    public TransactionNotFoundException(long id) {
        super("Transaction not found with id: " + id);
    }

    public TransactionNotFoundException(String description) {
        super("Transaction not found with this description: " + description);
    }
}
