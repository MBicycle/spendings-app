package com.finn.man.financeMan.exception;

/**
 * V.B. on
 * 09-Mar-18.
 */
public class ExpectedException extends RuntimeException {

    public ExpectedException(String message) {
        super(message);
    }
}
