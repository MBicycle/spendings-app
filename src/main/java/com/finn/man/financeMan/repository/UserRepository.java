package com.finn.man.financeMan.repository;

import com.finn.man.financeMan.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * V.B. on
 * 23-Feb-18.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    // false if not found
    boolean deleteById(long id);


    // null if not found
    Optional<User> getById(long id);

    // null if not found
    Optional<User> getByEmail(String email);

    List<User> findAll();

}
