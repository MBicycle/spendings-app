package com.finn.man.financeMan.service;

import com.finn.man.financeMan.exception.ExpectedException;
import com.finn.man.financeMan.exception.TransactionNotFoundException;
import com.finn.man.financeMan.model.*;
import com.finn.man.financeMan.repository.BalanceRepository;
import com.finn.man.financeMan.repository.CategoryRepository;
import com.finn.man.financeMan.repository.TransactionRepository;
import com.finn.man.financeMan.repository.UserRepository;
import com.finn.man.financeMan.to.transaction.CreateTransactionInputResource;
import com.finn.man.financeMan.to.transaction.UpdateTransactionInputResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

/**
 * V.B. on
 * 23-Feb-18.
 */
@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    private final BalanceRepository balanceRepository;

    private final UserRepository userRepository;

    private final CategoryRepository categoryRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, BalanceRepository balanceRepository, UserRepository userRepository, CategoryRepository categoryRepository) {
        this.transactionRepository = transactionRepository;
        this.balanceRepository = balanceRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
    }

    // null if transaction do not belong to userId
    @Transactional
    public Transaction add(CreateTransactionInputResource inputResource, long userId) {
        Assert.notNull(inputResource, "transaction is null");
        Optional<User> user = userRepository.getById(userId);

        return user.map(user1 -> {
            Transaction transaction = Transaction.builder()
                    .user(user1)
                    .operationType(defineType(inputResource.getOperationType()))
                    .category(defineCategory(inputResource.getCategory()))
                    .amount(inputResource.getAmount())
                    .description(inputResource.getDescription())
                    .build();

            Optional<Balance> balanceByOwner = balanceRepository.getByOwnerId(user.get().getId());
            balanceByOwner.ifPresent(balance -> balance.setAmount(setBalanceOnAddTransaction(balance, transaction)));
            return transactionRepository.save(transaction);
        }).orElseThrow(() -> new ExpectedException("user not exist in db"));
    }

    @Transactional
    public Transaction update(UpdateTransactionInputResource transactionInputResource, long userId) {
        Assert.notNull(transactionInputResource, "transaction input resource is null");

        Optional<Transaction> oldTransaction = findById(transactionInputResource.getId(), userId);
        oldTransaction.orElseThrow(TransactionNotFoundException::new);

        Transaction updated = Transaction.builder()
                .operationType(defineType(transactionInputResource.getOperationType()))
                .category(defineCategory(transactionInputResource.getCategory()))
                .amount(transactionInputResource.getAmount())
                .description(transactionInputResource.getDescription())
                .build();

        Optional<User> user = userRepository.getById(userId);
        user.ifPresent(user1 -> {
            Optional<Balance> balanceByOwner = balanceRepository.getByOwnerId(user1.getId());
            balanceByOwner.ifPresent(balance -> {
                balance.setAmount(setBalanceOnUpdateTransaction(balance, oldTransaction.get(), updated));
                balanceRepository.save(balance);
                updated.setId(oldTransaction.get().getId());
                updated.setUser(user1);
                updated.setAdded(oldTransaction.get().getAdded());
            });
        });
        return transactionRepository.save(updated);
    }


    public Optional<Transaction> findByDescriptionIsLike(String description, long userId) {
        return transactionRepository.findByDescriptionIsLikeAndUserId(description, userId);
    }

    // ORDERED dateTime desc
    public List<Transaction> findAll(long userId) {
        return transactionRepository.findAllByUserIdOrderByAddedDesc(userId);
    }

    // null if transaction do not belong to userId
    public Optional<Transaction> findById(long id, long userId) {
        return transactionRepository.findByIdAndUserId(id, userId);
    }

    // false if transaction do not belong to userId
    public boolean deleteById(long id, long userId) {
        Optional<Transaction> byIdAndUserId = transactionRepository.findByIdAndUserId(id, userId);
        if (byIdAndUserId.isPresent()) {
            Optional<Balance> balance = balanceRepository.getByOwnerId(userId);
            balance.ifPresent(balance1 -> balance1.setAmount(setBalanceOnDeleteTransaction(balance1, byIdAndUserId.get())));
            return transactionRepository.deleteTransactionById(id, userId) != 0;
        } else {
            throw new TransactionNotFoundException(id);
        }
    }

    private BigDecimal setBalanceOnDeleteTransaction(Balance balance, Transaction transaction) {
        if (transaction.getOperationType().equals(OperationType.INCOME)) {
            return balance.getAmount().subtract(transaction.getAmount());
        }
        if (transaction.getOperationType().equals(OperationType.OUTCOME)) {
            return balance.getAmount().add(transaction.getAmount());
        }
        return null;
    }

    private BigDecimal setBalanceOnAddTransaction(Balance balance, Transaction transaction) {
        if (transaction.getOperationType().equals(OperationType.INCOME)) {
            return balance.getAmount().add(transaction.getAmount());
        }
        if (transaction.getOperationType().equals(OperationType.OUTCOME)) {
            return balance.getAmount().subtract(transaction.getAmount());
        }
        return null;
    }

    private OperationType defineType(String operationType) {
        if (operationType.equals(OperationType.INCOME.name())) {
            return OperationType.INCOME;
        }
        if (operationType.equals(OperationType.OUTCOME.name())) {
            return OperationType.OUTCOME;
        } else throw new ExpectedException("operation type don't specified");
    }


    private Category defineCategory(String category) {
        Optional<Category> defaultCategory = Optional.of(new Category("default"));
        if (isNull(category)) {
            return categoryRepository.findByDescription(defaultCategory.get().getDescription()).orElse(defaultCategory.get());
        }
        return categoryRepository.findByDescription(category).orElse(new Category(category));
    }

    private BigDecimal setBalanceOnUpdateTransaction(Balance balance, Transaction old, Transaction newTransaction) {
        BigDecimal currentBalance = balance.getAmount();
        {
            if (old.getOperationType().equals(OperationType.INCOME)) {
                currentBalance = balance.getAmount().subtract(old.getAmount());
            } else if (old.getOperationType().equals(OperationType.OUTCOME)) {
                currentBalance = balance.getAmount().add(old.getAmount());
            }
        }

        {
            if (newTransaction.getOperationType().equals(OperationType.INCOME)) {
                currentBalance = currentBalance.add(newTransaction.getAmount());
            } else if (newTransaction.getOperationType().equals(OperationType.OUTCOME)) {
                currentBalance = currentBalance.subtract(newTransaction.getAmount());
            }
        }
        return currentBalance;
    }
}
