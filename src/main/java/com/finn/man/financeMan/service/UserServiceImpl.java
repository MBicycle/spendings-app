package com.finn.man.financeMan.service;

import com.finn.man.financeMan.model.Balance;
import com.finn.man.financeMan.model.User;
import com.finn.man.financeMan.repository.BalanceRepository;
import com.finn.man.financeMan.repository.UserRepository;
import com.finn.man.financeMan.to.user.CreateUserInputResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * V.B. on
 * 13-Mar-18.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final BalanceRepository balanceRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BalanceRepository balanceRepository) {
        this.userRepository = userRepository;
        this.balanceRepository = balanceRepository;
    }

    @Override
    public boolean deleteById(long id) {
        return userRepository.deleteById(id);
    }

    @Override
    public Optional<User> getById(long id) {
        return userRepository.getById(id);
    }

    @Override
    public Optional<User> getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User create(CreateUserInputResource userTO) {
        User user = User.builder()
                .name(userTO.getName())
                .email(userTO.getEmail())
                .age(userTO.getAge())
                .password(userTO.getPassword())
                .build();

        Balance balance = createBalance(user);
        balanceRepository.save(balance);
        user.setBalance(balance);
        user.setEnabled(true);
        return userRepository.save(user);
    }

    @Override
    public User update(CreateUserInputResource user) {
        return null;
    }

    private Balance createBalance(User user) {
        return Balance.builder()
                .amount(new BigDecimal(0))
                .owner(user)
                .build();
    }
}
