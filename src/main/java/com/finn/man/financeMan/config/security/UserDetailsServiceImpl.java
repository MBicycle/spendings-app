package com.finn.man.financeMan.config.security;

import com.finn.man.financeMan.exception.ExpectedException;
import com.finn.man.financeMan.model.User;
import com.finn.man.financeMan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * V.B. on
 * 31-Mar-18.
 */
@Service("UserDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Optional<User> user = userRepository.getByEmail(email);

        org.springframework.security.core.userdetails.User.UserBuilder builder;

        if (user.isPresent()) {

            String[] roles = {"USER"};

            builder = org.springframework.security.core.userdetails.User.withUsername(email);
            builder.password(user.get().getPassword());
            builder.roles(roles);
        } else {
            throw new ExpectedException("User not found.");
        }
        return builder.build();
    }
}