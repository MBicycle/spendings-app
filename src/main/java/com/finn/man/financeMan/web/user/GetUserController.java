package com.finn.man.financeMan.web.user;

import com.finn.man.financeMan.exception.ExpectedException;
import com.finn.man.financeMan.model.User;
import com.finn.man.financeMan.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * V.B. on
 * 13-Mar-18.
 */
@RestController
@RequestMapping("/users")
public class GetUserController {

    private static final Logger log = LoggerFactory.getLogger(GetUserController.class);

    private final UserService userService;

    @Autowired
    public GetUserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public User getUser(@PathVariable("id") long id) {
        return userService.getById(id).orElseThrow(() -> new ExpectedException("user not found"));
    }

    @GetMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public List<User> getAll() {
        log.debug("fetch users");
        return userService.findAll();
    }
}
