package com.finn.man.financeMan.web.user;

import com.finn.man.financeMan.service.UserService;
import com.finn.man.financeMan.to.user.CreateUserInputResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * V.B. on
 * 04-Apr-18.
 */
@RestController
@RequestMapping("/users")
public class SignUpUserController {

    private final UserService userService;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public SignUpUserController(UserService userService, BCryptPasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody CreateUserInputResource user) {
        user.setPassword(encoder.encode(user.getPassword()));
        userService.create(user);
    }

}
