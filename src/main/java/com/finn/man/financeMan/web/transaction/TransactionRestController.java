package com.finn.man.financeMan.web.transaction;

import com.finn.man.financeMan.exception.TransactionIdMismatchException;
import com.finn.man.financeMan.exception.TransactionNotFoundException;
import com.finn.man.financeMan.model.Transaction;
import com.finn.man.financeMan.service.TransactionService;
import com.finn.man.financeMan.to.transaction.CreateTransactionInputResource;
import com.finn.man.financeMan.to.transaction.UpdateTransactionInputResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * V.B. on
 * 06-Feb-18.
 */
@RestController
@RequestMapping("/transactions")
public class TransactionRestController {

    private static final long USER_ID = 1;

    private static final Logger log = LoggerFactory.getLogger(TransactionRestController.class);

    private final TransactionService transactionService;

    @Autowired
    public TransactionRestController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping
    public Iterable getAll() {
        return transactionService.findAll(USER_ID);
    }

    @GetMapping("/{description}")
    public Transaction getByDescription(@PathVariable("description") String description) {
        log.debug(" get transaction by description {}", description);
        return transactionService.findByDescriptionIsLike(description, USER_ID).orElseThrow(() -> new TransactionNotFoundException(description));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        log.debug("delete transaction by id {}", id);
        transactionService.deleteById(id, USER_ID);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Transaction create(@RequestBody @Valid CreateTransactionInputResource inputResource) {
        log.info("creating transaction");
        return transactionService.add(inputResource, USER_ID);
    }

    @PutMapping("/{id}")
    public Transaction update(@RequestBody @Valid UpdateTransactionInputResource transaction, @PathVariable("id") Long id) {
        log.info(" update transaction with id {}", id);
        if (id.longValue() != transaction.getId()) throw new TransactionIdMismatchException("transaction id mismatch: " + transaction.getId() + ":" + id);
        return transactionService.update(transaction, USER_ID);
    }

}
