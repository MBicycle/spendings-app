package com.finn.man.financeMan.web.user;

import com.finn.man.financeMan.model.User;
import com.finn.man.financeMan.service.UserService;
import com.finn.man.financeMan.to.user.CreateUserInputResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * V.B. on
 * 13-Mar-18.
 */
@RestController
@RequestMapping("/users")
public class CreateUserController {

    private final UserService userService;

    @Autowired
    public CreateUserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody @Valid CreateUserInputResource userInputResource) {
        return userService.create(userInputResource);
    }
}
